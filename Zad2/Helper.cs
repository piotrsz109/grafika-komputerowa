﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Zad2 {
    public static class Helper {
        public static ImageDto? ReadPPM3File(string filePath)
        {
            try
            {
                using var fileStream = new FileStream(filePath, FileMode.Open);
                using var reader = new StreamReader(fileStream);

                var formatIdentifier = ReadLineIgnoreComment(reader);
                if (formatIdentifier != "P3")
                    throw new Exception("Invalid PPM file format. It must be in P3 format.");

                var dimensionsLine = ReadLineIgnoreComment(reader);

                if (dimensionsLine is null)
                    throw new Exception("Incomplete file");

                var dimensions = dimensionsLine.Split(' ').Where(x => int.TryParse(x, out _)).ToArray();

                var width = 0;
                var height = 0;
                var maxColorValue = 0;
                var dataOmmited = "";

                if (dimensions.Length == 2)
                {
                    width = int.Parse(dimensions[0]);
                    height = int.Parse(dimensions[1]);
                }
                else
                {
                    width = int.Parse(dimensions[0]);
                    dimensionsLine = ReadLineIgnoreComment(reader);
                    dimensions = ParseStringToEnd(dimensionsLine).Split(' ');
                    var tt = dimensions.Where(x => int.TryParse(x, out _)).ToList();
                    height = int.Parse(tt[0]);
                    if (tt.Count > 1)
                        maxColorValue = int.Parse(tt[1]);

                    dataOmmited = string.Join(" ", tt.Select((x, index) => new { x, index }).Where(x => x.index > 1).Select(x => x.x));
                }

                if (width == 0 || height == 0)
                    throw new Exception("Invalid dimensions in the PPM file.");

                if (maxColorValue == 0)
                {
                    var maxColorValueLine = ReadLineIgnoreComment(reader);

                    if (maxColorValueLine is null)
                        throw new Exception("Incomplete file");

                    maxColorValue = int.Parse(maxColorValueLine);
                }

                var builder = new StringBuilder();
                builder.Append(dataOmmited + " ");
                int read = 0;

                do
                {
                    var buffer = new char[1024 * 1024];
                    read = reader.ReadBlock(buffer, 0, buffer.Length);
                    builder.Append(read == buffer.Length ? buffer : buffer.AsSpan().Slice(0, read));
                } while (!reader.EndOfStream);

                var data = builder.ToString();

                data = ParseStringToEnd(data);

                var pixels = new List<Color>(width * height + 1);

                var sliced = data.Split(' ', StringSplitOptions.RemoveEmptyEntries);

                var asArray = new string[3];
                
                for (int i = 0; i < sliced.Length; i += 3)
                {
                    asArray[0] = sliced[i];
                    asArray[1] = sliced[i + 1];
                    asArray[2] = sliced[i + 2];
                    
                    try
                    {
                        pixels.Add(new Color()
                        {
                            R = (byte)(int.Parse(asArray[0]) / (maxColorValue / 255)),
                            G = (byte)(int.Parse(asArray[1]) / (maxColorValue / 255)),
                            B = (byte)(int.Parse(asArray[2]) / (maxColorValue / 255)),
                            A = byte.MaxValue,
                        });
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }

                return new ImageDto()
                {
                    MaxColorValue = maxColorValue,
                    Width = width,
                    Height = height,
                    Pixels = pixels,
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        public static ImageDto? ReadPPM6File(string filePath)
        {
            try
            {
                using var fileStream = new FileStream(filePath, FileMode.Open);
                using var reader = new StreamReader(fileStream);

                var formatIdentifier = ReadLineIgnoreComment(reader);
                if (formatIdentifier != "P6")
                    throw new Exception("Invalid PPM file format. It must be in P6 format.");

                var dimensionsLine = ReadLineIgnoreComment(reader);

                if (dimensionsLine is null)
                    throw new Exception("Incomplete file");

                var dimensions = dimensionsLine.Split(' ').Where(x => int.TryParse(x, out _)).ToArray();

                var width = 0;
                var height = 0;
                var maxColorValue = 0;
                if (dimensions.Length == 2)
                {
                    width = int.Parse(dimensions[0]);
                    height = int.Parse(dimensions[1]);
                }
                else
                {
                    width = int.Parse(dimensions[0]);
                    dimensionsLine = ReadLineIgnoreComment(reader);
                    dimensions = ParseStringToEnd(dimensionsLine).Split(' ');
                    var temp = dimensions.Where(x => int.TryParse(x, out _)).ToList();
                    height = int.Parse(temp[0]);
                    if (temp.Count > 1)
                        maxColorValue = int.Parse(temp[1]);
                }

                if (width == 0 || height == 0)
                    throw new Exception("Invalid dimensions in the PPM file.");

                if (maxColorValue == 0)
                {
                    var maxColorValueLine = ReadLineIgnoreComment(reader);

                    if (maxColorValueLine is null)
                        throw new Exception("Incomplete file");

                    maxColorValue = int.Parse(maxColorValueLine);
                }

                var pixels = new List<Color>(width * height + 1);

                int pixelSize = maxColorValue <= byte.MaxValue ? 3 : 6;

                int read = 0;

                var builder = new StringBuilder();

                do
                {
                    var buffer = new char[1024];
                    read = reader.ReadBlock(buffer, 0, buffer.Length);
                    builder.Append(read == buffer.Length ? buffer : buffer.AsSpan().Slice(0, read));
                } while (!reader.EndOfStream);

                var data = builder.ToString();
                if (data.Length < width * height * pixelSize)
                    throw new Exception("Incomplete file!");

                var asArray = new char[pixelSize];

                for (int i = 0; i < data.Length; i += pixelSize)
                {
                    for (int j = 0; j < pixelSize; j++)
                    {
                        asArray[j] = data[i + j];
                    }

                    try
                    {
                        if (pixelSize == 3)
                        {
                            pixels.Add(new Color()
                            {
                                R = (byte)((byte)asArray[0] / (maxColorValue / 255)),
                                G = (byte)((byte)asArray[1] / (maxColorValue / 255)),
                                B = (byte)((byte)asArray[2] / (maxColorValue / 255)),
                                A = byte.MaxValue,
                            });
                        }
                        else
                        {
                            pixels.Add(new Color()
                            {
                                R = (byte)(asArray[0] * asArray[1] / (maxColorValue / 255)),
                                G = (byte)(asArray[2] * asArray[3] / (maxColorValue / 255)),
                                B = (byte)(asArray[4] * asArray[5] / (maxColorValue / 255)),
                                A = byte.MaxValue,
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }

                if (pixels.Count != width * height)
                {
                    throw new Exception("Incomplete file");
                }

                return new ImageDto()
                {
                    MaxColorValue = maxColorValue,
                    Width = width,
                    Height = height,
                    Pixels = pixels,
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        public static char DetectSeparator(string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                if ((text[i] < '0' || text[i] > '9') && !Environment.NewLine.Contains(text[i]) && text[i] != '#')
                    return text[i];
            }

            return ' ';
        }

        public static string ParseStringToEnd(string text)
        {
            var result = new List<char>();

            foreach (var item in text)
            {
                if ((item >= '0' && item <= '9'))
                    result.Add(item);

                if (char.IsWhiteSpace(item))
                    result.Add(' ');
            }

            return string.Join("", result);
        }

        private static string? ReadLineIgnoreComment(StreamReader reader)
        {
            string temp = null;
            do
            {
                temp = reader.ReadLine();
            } while (temp?.Length > 0 && temp[0] == '#' || string.IsNullOrWhiteSpace(temp));

            return temp;
        }
    }
}