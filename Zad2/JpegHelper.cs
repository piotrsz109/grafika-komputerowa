﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using Color = System.Windows.Media.Color;

namespace Zad2 {
    public class JpegHelper {
        public static (ImageDto? dto, Bitmap bitmap)? ReadJPEGFile(string filePath)
        {
            try
            {
                Bitmap bitmap = new Bitmap(filePath);
                int width = bitmap.Width;
                int height = bitmap.Height;
                Color[] colors = new Color[width * height];

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        colors[y * width + x] = Color.FromArgb(
                            bitmap.GetPixel(x, y).A,
                            bitmap.GetPixel(x, y).R,
                            bitmap.GetPixel(x, y).G,
                            bitmap.GetPixel(x, y).B
                        );
                    }
                }

                return (new ImageDto()
                {
                    Width = width,
                    Height = height,
                    Pixels = colors,
                    MaxColorValue = byte.MaxValue,
                }, bitmap);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return null;
            }
        }

        public static void SaveAsJPEG(ImageDto image, string filePath)
        {
            try
            {
                using (Bitmap bitmap = new Bitmap(image.Width, image.Height))
                {
                    for (int y = 0; y < image.Height; y++)
                    {
                        for (int x = 0; x < image.Width; x++)
                        {
                            var pixel = image.Pixels.ElementAt(y * image.Width + x);
                            bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(pixel.A, pixel.R, pixel.G, pixel.B));
                        }
                    }

                    bitmap.Save(filePath, ImageFormat.Jpeg);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }
    }
}