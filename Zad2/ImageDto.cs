﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace Zad2 {
    public class ImageDto {
        public int Width { get; set; }
        public int Height { get; set; }
        public int MaxColorValue { get; set; }

        public IReadOnlyCollection<Color> Pixels { get; set; } = Array.Empty<Color>();
    }
}