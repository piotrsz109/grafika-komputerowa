﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace Zad2 {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged {
        public event PropertyChangedEventHandler? PropertyChanged;
        
        private ImageDto? CurrentFile { get; set; } = null;
        private byte[] userImage;

        public byte[] UserImage
        {
            get { return userImage; }
            set
            {
                if (value != userImage)
                {
                    userImage = value;
                    OnPropertyChanged("UserImage");
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadP3File(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();

            if (dialog.ShowDialog() != true)
                return;
            
            var filePath = dialog.FileName;

            var file = Helper.ReadPPM3File(filePath);

            if (file is null)
                return;
            
            CurrentFile = file;
            var stride = (file.Width * PixelFormats.Rgb24.BitsPerPixel) / 8;
            PreviewImage.Source = BitmapSource.Create(
                file.Width, file.Height, 96, 96, PixelFormats.Rgb24, null, 
                GetByteArray(file.Pixels), stride);

            PreviewImage.Width = file.Width;
            PreviewImage.Height = file.Height;
        }
        
        private void LoadP6File(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();

            if (dialog.ShowDialog() != true)
                return;
            
            var filePath = dialog.FileName;

            var file = Helper.ReadPPM6File(filePath);

            if (file is null)
                return;

            CurrentFile = file;
            var stride = (file.Width * PixelFormats.Rgb24.BitsPerPixel) / 8;
            PreviewImage.Source = BitmapSource.Create(
                file.Width, file.Height, 96, 96, PixelFormats.Rgb24, null, 
                GetByteArray(file.Pixels), stride);

            PreviewImage.Width = file.Width;
            PreviewImage.Height = file.Height;
        }
        
        private void LoadJpegFile(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();

            if (dialog.ShowDialog() != true)
                return;
            
            var filePath = dialog.FileName;

            var file = JpegHelper.ReadJPEGFile(filePath);

            if (file is null)
                return;
            
            CurrentFile = file.Value.dto;

            PreviewImage.Source = Convert(file.Value.bitmap);
        }

        private void SaveJpegFile(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog
            {
                Filter = "Jpeg Files (*.jpg)|*.jpg"
            };

            if (CurrentFile is not null && dialog.ShowDialog() != true)
                return;

            var filePath = dialog.FileName;

            JpegHelper.SaveAsJPEG(CurrentFile!, filePath);
        }

        private byte[] GetByteArray(IReadOnlyCollection<Color> pixels)
        {
            var result = new List<byte>();

            foreach (var item in pixels)
            {
                result.Add(item.R);
                result.Add(item.G);
                result.Add(item.B);
            }

            return result.ToArray();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        
        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }
        
        private static BitmapSource Convert(System.Drawing.Bitmap bitmap)
        {
            var bitmapData = bitmap.LockBits(
                new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

            var bitmapSource = BitmapSource.Create(
                bitmapData.Width, bitmapData.Height,
                bitmap.HorizontalResolution, bitmap.VerticalResolution,
                PixelFormats.Bgr24, null,
                bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);

            bitmap.UnlockBits(bitmapData);

            return bitmapSource;
        }
    }
}