﻿using System.Windows;
using Zad1.DataContracts;

namespace Zad1
{
    public partial class AddShape : Window
    {
        private readonly AddShapeRequest _request;
        
        public AddShape(AddShapeRequest request)
        {
            InitializeComponent();

            _request = request;

            switch (request.ShapeType)
            {
                case ShapeType.Line:
                    AddLineGrid.Visibility = Visibility.Visible;
                    break;
                case ShapeType.Rectangle:
                    AddRectangleGrid.Visibility = Visibility.Visible;
                    break;
                case ShapeType.Circle:
                    AddCircleGrid.Visibility = Visibility.Visible;
                    break;
            }
        }

        private void AddLine(object sender, RoutedEventArgs e)
        {
            _request.Callback.Invoke(new AddLineResponse() {
                Origin = new Point(double.Parse(LineX1TextBox.Text), double.Parse(LineY1TextBox.Text)),
                End = new Point(double.Parse(LineX2TextBox.Text), double.Parse(LineY2TextBox.Text)),
            });
            
            this.Close();
        }

        private void AddRectangle(object sender, RoutedEventArgs e)
        {
            _request.Callback.Invoke(new AddRectangleResponse() {
                Origin = new Point(double.Parse(RectangleX1TextBox.Text), double.Parse(RectangleY1TextBox.Text)),
                Width = double.Parse(RectangleWidthTextBox.Text),
                Height = double.Parse(RectangleHeightTextBox.Text),
            });
            
            this.Close();
        }

        private void AddCircle(object sender, RoutedEventArgs e)
        {
            _request.Callback.Invoke(new AddCircleResponse() {
                Origin = new Point(double.Parse(CircleX1TextBox.Text), double.Parse(CircleY1TextBox.Text)),
                Radius = double.Parse(CircleRadiusTextBox.Text),
            });
            
            this.Close();
        }
    }
}