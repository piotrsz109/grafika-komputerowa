﻿using System.Windows;
using System.Windows.Shapes;

namespace Zad1
{
    public static class Extenstions
    {
        public static void SetStartPoint(this Line line, Point point)
        {
            line.X1 = point.X;
            line.Y1 = point.Y;
        }
        public static void SetEndPoint(this Line line, Point point)
        {
            line.X2 = point.X;
            line.Y2 = point.Y;
        } 
    }
}