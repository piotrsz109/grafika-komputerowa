﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Zad1.DataContracts
{
    public class AddShapeResponse
    {
        public Point Origin { get; set; }

        public virtual Shape ToShape()
        {
            throw new Exception();
        }
    }

    public class AddLineResponse : AddShapeResponse
    {
        public Point End { get; set; }

        public override Shape ToShape()
        {
            return new Line() {
                Stroke = Brushes.Indigo,
                StrokeThickness = 5,
                Visibility = Visibility.Visible,
                X1 = Origin.X,
                Y1 = Origin.Y,
                X2 = End.X,
                Y2 = End.Y,
            };
        }
    }

    public class AddRectangleResponse : AddShapeResponse
    {
        public double Width { get; set; }
        public double Height { get; set; }
        public override Shape ToShape()
        {
            var result =  new Rectangle() {
                Stroke = Brushes.Indigo,
                StrokeThickness = 5,
                Visibility = Visibility.Visible,
                Width = Width,
                Height = Height,
            };
            
            Canvas.SetLeft(result, Origin.X);
            Canvas.SetTop(result, Origin.Y);

            return result;
        }
    }

    public class AddCircleResponse : AddShapeResponse
    {
        public double Radius { get; set; }
        public override Shape ToShape()
        {
            var result =  new Ellipse() {
                Stroke = Brushes.Indigo,
                StrokeThickness = 5,
                Visibility = Visibility.Visible,
                Width = Radius,
                Height = Radius,
            };
            
            Canvas.SetLeft(result, Origin.X);
            Canvas.SetTop(result, Origin.Y);

            return result;
        }
    }
}