﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zad1.DataContracts
{
    public class ShapesToSave
    {
        public IEnumerable<AddLineResponse> Lines { get; set; } = Array.Empty<AddLineResponse>();
        public IEnumerable<AddRectangleResponse> Rectangles { get; set; } = Array.Empty<AddRectangleResponse>();
        public IEnumerable<AddCircleResponse> Circles { get; set; } = Array.Empty<AddCircleResponse>();

        public IEnumerable<AddShapeResponse> Shapes => Lines.Select(x => x as AddShapeResponse).Union(Rectangles).Union(Circles);
    }
}