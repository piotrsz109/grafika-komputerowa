﻿using System;

namespace Zad1.DataContracts
{
    public class AddShapeRequest
    {
        public ShapeType ShapeType { get; set; }
        public Action<AddShapeResponse> Callback { get; set; } = null!;
    }
}