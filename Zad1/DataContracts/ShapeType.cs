﻿namespace Zad1.DataContracts
{
    public enum ShapeType
    {
        Unknown = 0,
        Line = 1,
        Rectangle = 2,
        Circle = 3,
    }
}