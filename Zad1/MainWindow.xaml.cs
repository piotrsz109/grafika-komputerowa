﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Zad1.DataContracts;

namespace Zad1
{
    public partial class MainWindow
    {
        private readonly List<Shape> _shapes = new();
        private Shape? _currentShape;
        private bool _isDrawing;
        private Point _startPoint;

        private bool IsDragging => DragRadioButton.IsChecked == true && _currentShape != null;

        public MainWindow()
        {
            InitializeComponent();
            Canvas.Background = new SolidColorBrush(Colors.LightGray);
        }

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && !IsDragging && DragRadioButton.IsChecked != true)
            {
                _startPoint = e.GetPosition(Canvas);

                if (_currentShape == null)
                {
                    if (LineRadioButton.IsChecked == true)
                    {
                        _currentShape = new Line() {
                            Stroke = Brushes.Black,
                            StrokeThickness = 5,
                            Visibility = Visibility.Visible,
                        };

                        ((Line)_currentShape).SetStartPoint(_startPoint);
                    }
                    else if (RectangleRadioButton.IsChecked == true)
                    {
                        _currentShape = new Rectangle() {
                            Stroke = Brushes.Black,
                            StrokeThickness = 5,
                            Visibility = Visibility.Visible,
                        };

                        Canvas.SetLeft(_currentShape, _startPoint.X);
                        Canvas.SetTop(_currentShape, _startPoint.Y);
                    }
                    else if (CircleRadioButton.IsChecked == true)
                    {
                        _currentShape = new Ellipse() {
                            Stroke = Brushes.Black,
                            StrokeThickness = 5,
                            Visibility = Visibility.Visible,
                        };

                        Canvas.SetLeft(_currentShape, _startPoint.X);
                        Canvas.SetTop(_currentShape, _startPoint.Y);
                    }
                }

                if (_currentShape is null) return;

                _isDrawing = true;
                _currentShape.MouseDown += ShapeOnMouseDown;
                _shapes.Add(_currentShape);
                Canvas.Children.Add(_currentShape);
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            Point currentMousePosition = e.GetPosition(Canvas);

            if (_isDrawing)
            {
                switch (_currentShape)
                {
                    case Line line:
                        line.SetEndPoint(currentMousePosition);
                        break;
                    case Rectangle rectangle: {
                        var width = currentMousePosition.X - _startPoint.X;
                        var height = currentMousePosition.Y - _startPoint.Y;
                        rectangle.Width = Math.Abs(width);
                        rectangle.Height = Math.Abs(height);
                        break;
                    }
                    case Ellipse ellipse: {
                        var width = currentMousePosition.X - _startPoint.X;
                        ellipse.Width = Math.Abs(width);
                        ellipse.Height = Math.Abs(width);
                        break;
                    }
                }
            }
            else if (IsDragging)
            {
                Canvas.SetLeft(_currentShape!, currentMousePosition.X);
                Canvas.SetTop(_currentShape!, currentMousePosition.Y);
            }
        }

        private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _isDrawing = false;
            _currentShape = null;
        }

        private void AddShape(object sender, RoutedEventArgs e)
        {
            var dialog = new AddShape(new AddShapeRequest() {
                ShapeType = GetShapeType(),
                Callback = AddShapeToCanvasFromResponse,
            });

            dialog.Show();
        }

        private void AddShapeToCanvasFromResponse(AddShapeResponse res)
        {
            var shape = res.ToShape();
            _shapes.Add(shape);
            shape.MouseDown += ShapeOnMouseDown;
            Canvas.Children.Add(shape);
        }

        private ShapeType GetShapeType()
        {
            if (RectangleRadioButton.IsChecked == true)
            {
                return ShapeType.Rectangle;
            }

            if (CircleRadioButton.IsChecked == true)
            {
                return ShapeType.Circle;
            }

            return ShapeType.Line;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Canvas.Children.Clear();
            _shapes.Clear();
            _currentShape = null;

            Canvas.UpdateLayout();
        }

        private void ShapeOnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (DragRadioButton.IsChecked == true && _currentShape is null)
            {
                _currentShape = sender as Shape;
            }
        }

        private void SaveToFile(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog {
                Filter = "Json Files (*.json)|*.json"
            };

            if (dialog.ShowDialog() == true)
            {
                var filePath = dialog.FileName;

                var temp = new ShapesToSave() {
                    Lines = _shapes.Where(x => x is Line).Select(x => (GetDto(x) as AddLineResponse)!),
                    Rectangles = _shapes.Where(x => x is Rectangle).Select(x => (GetDto(x) as AddRectangleResponse)!),
                    Circles = _shapes.Where(x => x is Ellipse).Select(x => (GetDto(x) as AddCircleResponse)!),
                };
                
                var json = JsonConvert.SerializeObject(temp);

                File.WriteAllText(filePath, json);
            }
        }

        private AddShapeResponse? GetDto(Shape shape)
        {
            switch (shape)
            {
                case Line line:
                    return new AddLineResponse() {
                        Origin = new Point(line.X1, line.Y1),
                        End = new Point(line.X2, line.Y2),
                    };
                case Rectangle rectangle:
                    return new AddRectangleResponse() {
                        Origin = new Point(Canvas.GetLeft(shape), Canvas.GetTop(shape)),
                        Width = rectangle.Width,
                        Height = rectangle.Height,
                    };
                case Ellipse ellipse:
                    return new AddCircleResponse() {
                        Origin = new Point(Canvas.GetLeft(shape), Canvas.GetTop(shape)),
                        Radius = ellipse.Width,
                    };
            }

            return null;
        }

        private void ReadFromFile(object sender, RoutedEventArgs e)
        {
            ClearButton_Click(null, null);

            var dialog = new OpenFileDialog() {
                Filter = "Json Files (*.json)|*.json"
            };

            if (dialog.ShowDialog() == true)
            {
                var filePath = dialog.FileName;

                var json = File.ReadAllText(filePath);

                var objects = JsonConvert.DeserializeObject<ShapesToSave>(json);

                if (objects is null)
                    return;

                foreach (var item in objects.Shapes)
                {
                    AddShapeToCanvasFromResponse(item);
                }
            }
        }
    }
}